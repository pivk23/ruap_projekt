from sklearn.ensemble import RandomForestClassifier, GradientBoostingClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import confusion_matrix
import pandas as pd


pd.set_option('max_columns', None)

df_train = pd.read_csv('data/train_esrb.csv')
df_test = pd.read_csv('data/test_esrb.csv')


def preprocess_input(df, scaler, split='train'):
    df = df.copy()

    # drop title column
    df = df.drop('title', axis=1)

    # shuffle the data
    df = df.sample(frac=1.0, random_state=1).reset_index(drop=True)

    # split df into x and y
    y = df['esrb_rating']
    x = df.drop('esrb_rating', axis=1)

    # scale x
    if split == 'train':
        scaler.fit(x)
    x = pd.DataFrame(scaler.transform(x), index=x.index, columns=x.columns)

    return x, y


scaler = StandardScaler()

x_train, y_train = preprocess_input(df_train, scaler=scaler, split='train')
x_test, y_test = preprocess_input(df_test, scaler=scaler, split='test')

models = {
    "Logistic Regression": LogisticRegression(),
    "      Decision Tree": DecisionTreeClassifier(),
    "      Random Forest": RandomForestClassifier(),
    "K-Nearest Neighbors": KNeighborsClassifier(),
    "     SVM Classifier": SVC(),
    "  Gradient Boosting": GradientBoostingClassifier(),
    "     Neural Network": MLPClassifier()
}

for name, model in models.items():
    model.fit(x_train, y_train)
    print(name + " trained.")

for name, model in models.items():
    print(
        name + " Accuracy: {:.2f}%".format(model.score(x_test, y_test) * 100))


r'''
model = RandomForestClassifier()
model.fit(x_train, y_train)
y_pred = model.predict(x_test)

print(confusion_matrix(y_test, y_pred))
'''
